//
//  AppDelegate+Setup.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 29/11/2020.
//

import Foundation
import IQKeyboardManagerSwift

extension AppDelegate {
    
    
    
    func setupIQKeyboardManagerSetup() {
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done"//L10n.General.done()
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.toolbarTintColor = UIColor.darkMint
        IQKeyboardManager.shared.shouldToolbarUsesTextFieldTintColor = false
    }
    
    func startLocalization() {
//        LocalizeService.shared.startService()
        // Configure Fonts
        UILabel.appearance().substituteFontName = mainFontName
        UITextView.appearance().substituteFontName = mainFontName
        UITextField.appearance().substituteFontName = mainFontName
        UIFont.overrideInitialize()
    }
    
    func setRootViewController() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = UIColor.white
        self.window?.makeKeyAndVisible()
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let splashController = mainStoryBoard.instantiateViewController(withIdentifier: "SplashViewController")
//        UIApplication.shared.restartTo(mainNavigationController)
        window?.rootViewController = splashController
    }
}

