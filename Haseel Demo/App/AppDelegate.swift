//
//  AppDelegate.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 28/11/2020.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let mainFontName = "Poppins"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.startLocalization()
        self.setupIQKeyboardManagerSetup()
        self.setRootViewController()
        
        return true
    }
}

