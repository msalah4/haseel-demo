//
//  UIImage+Base64.swift
//  AJEL
//
//  Created by Michelle Gerges on 5/19/18.
//  Copyright © 2018 Michelle Gerges. All rights reserved.
//

import UIKit

extension UIImage {
    
    func toPNGBase64() -> String? {
        return self.pngData()?.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    func toJPEGBase64( toImgeQuality: CGFloat) -> String? {
        return self.jpegData(compressionQuality: toImgeQuality)?
            .base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    func toCompressedData( afterCompressingQuality: CGFloat) -> Data? {
        return self.jpegData(compressionQuality: afterCompressingQuality)
    }
    
    func toJPEGBase64(maxKB maxSize: Int) -> String? {
        
        var imgData: Data? = self.toCompressedData(afterCompressingQuality: 1)
        
        if imgData == nil {
            return nil
        }
        
        while imgData?.sizeInKB() ?? 0 > maxSize {
            
            if let dataToBeCompressd = imgData ,
                let compressedData = UIImage(data: dataToBeCompressd)?
                    .toCompressedData(afterCompressingQuality: 0.9) {
                
                imgData = compressedData
            } else {
                imgData = nil
            }
        }
        
        return imgData?.toBase64String(imageExtesion: "")
    }
}
