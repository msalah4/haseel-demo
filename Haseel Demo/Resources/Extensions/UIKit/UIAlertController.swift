//
//  UIAlertController+Style.swift
//  Customer IOS
//
//  Created by Michele on 12/10/19.
//  Copyright © 2019 Michelle. All rights reserved.
//

import UIKit

extension UIAlertController {
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.view.tintColor = UIColor.darkMint
    }
    
    class func confirmAlertController(message: String?,
                                      acceptTitle: String = "Ok",//L10n.General.accept(),
                                      cancelTitle: String = "Cancel",//L10n.General.cancel(),
                                      handler: @escaping (() -> Void)) -> UIAlertController {
        let alertVC = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        alertVC.addAction(UIAlertAction(title: acceptTitle,
                                        style: .default, handler: { _ in
                                            handler()
        }))
        
        alertVC.addAction(UIAlertAction(title: cancelTitle,
                                        style: .cancel,
                                        handler: { _ in
                                            alertVC.dismiss(animated: true, completion: nil)
        }))
        
        return alertVC
    }
}
