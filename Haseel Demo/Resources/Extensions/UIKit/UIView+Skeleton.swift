//
//  UIView+Skeleton.swift
//  Customer IOS
//
//  Created by Michele on 2/16/20.
//  Copyright © 2020 Michelle. All rights reserved.
//

import Foundation
import UIKit
import SkeletonView

extension UIView {
    func startSkeletonAnimation() {
        self.showAnimatedGradientSkeleton(usingGradient: SkeletonAppearance.default.gradient,
                                          animation: GradientDirection.topLeftBottomRight.slidingAnimation(),
                                          transition: SkeletonTransitionStyle.none)
    }
    
    func stopSkeletonAnimation() {
        self.hideSkeleton(reloadDataAfter: true,
                          transition: SkeletonTransitionStyle.crossDissolve(0.5))
    }
}

extension UITableView {
    
    func registerSkeleton(cellClass: UITableViewCell.Type) {
        self.register(UINib(nibName: cellClass.cellNibName, bundle: nil),
                      forCellReuseIdentifier: cellClass.cellSkeletonIdentifier)
    }
    
    func registerSkeleton(viewClass: UITableViewHeaderFooterView.Type) {
        self.register(UINib(nibName: viewClass.viewNibName, bundle: nil),
                      forCellReuseIdentifier: viewClass.viewSkeletonIdentifier)
    }
}

extension UITableViewCell {
    
    static var cellSkeletonIdentifier: String {
        return self.cellIdentifier + "+Skeleton"
    }
    
    var isSkeletonCell: Bool {
        return (self.reuseIdentifier == Self.cellSkeletonIdentifier)
    }
}

extension UICollectionView {
    
    func registerSkeleton(cellClass: UICollectionViewCell.Type) {
        self.register(UINib(nibName: cellClass.cellNibName, bundle: nil),
                      forCellWithReuseIdentifier: cellClass.cellSkeletonIdentifier)
    }
}

extension UICollectionViewCell {
    
    static var cellSkeletonIdentifier: String {
        return self.cellIdentifier + "+Skeleton"
    }
    
    var isSkeletonCell: Bool {
        return (self.reuseIdentifier == Self.cellSkeletonIdentifier)
    }
}

extension UITableViewHeaderFooterView {
    
    static var viewSkeletonIdentifier: String {
        return self.viewIdentifier + "+Skeleton"
    }
}
