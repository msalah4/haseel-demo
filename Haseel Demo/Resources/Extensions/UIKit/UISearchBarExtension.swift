//
//  UISearchBarExtension.swift
//  Customer IOS
//
//  Created by Michele on 10/20/19.
//  Copyright © 2019 Michelle. All rights reserved.
//

import UIKit

extension UISearchBar {
    
    var textField: UITextField? {
        self.textFieldFrom(view: self)
    }
    
    private func textFieldFrom(view: UIView) -> UITextField? {
        
        if view is UITextField {
            return view as? UITextField
        }
        
        for subview in view.subviews {
            if let textField = textFieldFrom(view: subview) {
                return textField
            }
        }
        
        return nil
    }
}
