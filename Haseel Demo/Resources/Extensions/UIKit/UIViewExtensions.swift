//
//  UIViewExtensions.swift
//  GDP
//
//  Created by Michelle Gerges on 9/14/17.
//  Copyright © 2017 Michelle Gerges. All rights reserved.
//

import UIKit

public enum UIViewRoundedSide:String {
    case topLeft, bottomLeft, topRight, bottomRight, all
}

extension UIView {
    
    func makeRoundedCornerForSides(sides: [UIViewRoundedSide], radius: CGFloat = -1.0) {
        layer.cornerRadius = radius == CGFloat(-1.0) ? cornerRadius: radius
        var maskedCorners: CACornerMask = CACornerMask.init()
        
        for side in sides {
            if side == UIViewRoundedSide.all {
                layer.masksToBounds = cornerRadius > 0
            } else if side == UIViewRoundedSide.topLeft {
                maskedCorners.insert(.layerMinXMinYCorner)
            } else if side == UIViewRoundedSide.topRight {
                maskedCorners.insert(.layerMaxXMinYCorner)
            } else if side == UIViewRoundedSide.bottomLeft {
                maskedCorners.insert(.layerMinXMaxYCorner)
            } else if side == UIViewRoundedSide.bottomRight {
                maskedCorners.insert(.layerMaxXMaxYCorner)
            }
        }
        self.layer.maskedCorners = maskedCorners
    }

    
    func animateHeartbeating() {
        
        let animation = CASpringAnimation(keyPath: "transform.scale")
        
        animation.duration = 0.25
        
        animation.fromValue = 1.0
        animation.toValue = 1.25
        
        animation.autoreverses = true
        
        animation.initialVelocity = 0.5
        animation.damping = 0.8
        
        self.layer.add(animation, forKey: "beating")
    }
    
    func animateShakeing() {
        
        let animation = CASpringAnimation(keyPath: "transform.translation.x")
        
        animation.fromValue = 0
        animation.toValue = 10
        
        animation.duration = 0.1
        animation.repeatCount = 2
        animation.autoreverses = true
        
        animation.initialVelocity = 0.0
        animation.damping = 0.5
        
        self.layer.add(animation, forKey: "shaking")
    }
    
    var isHiddenIfNeeded: Bool {
        get {
            return self.isHidden
        }
        set {
            if self.isHidden != newValue {
                self.isHidden = newValue
            }
        }
    }
    
    func hideAnimated( completion: (() -> Void)? = nil ) {
        
        if self.isHidden { return }
        if self.superview is UIStackView {
            UIView.animate(withDuration: 0.25, animations: {
                self.isHidden = true
            }, completion: { _ in
                completion?()
            })
            return
        }
        
        self.alpha = 1
        self.isHidden = false
        
        UIView.animate(withDuration: 0.25, animations: {
            self.alpha = 0
        }, completion: { _ in
            self.alpha = 1
            self.isHidden = true
            completion?()
        })
    }
    
    func showupAnimated(_ completion:(() -> Void)? = nil) {
        
        if !self.isHidden { return }
        if self.superview is UIStackView {
            UIView.animate(withDuration: 0.25, animations: {
                self.isHidden = false
            }, completion: { _ in
                completion?()
            })
            return
        }
        self.alpha = 0
        self.isHidden = false
        
        UIView.animate(withDuration: 0.25, animations: {
            self.alpha = 1
        }, completion: { _ in
            completion?()
        })
    }
    
    var width: CGFloat {
        return self.bounds.width
    }
    
    var height: CGFloat {
        return self.bounds.height
    }
    
    func loadFromNib(addConstrains: Bool) {
        
        guard let view = Bundle.main.loadNibNamed(String(describing: type(of: self)),
                                                  owner: self,
                                                  options: nil)?.first as? UIView else { return }
        view.frame = self.bounds
        self.addSubview(view)
        
        if addConstrains {
            self.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                self.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                self.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                self.topAnchor.constraint(equalTo: view.topAnchor),
                self.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
        }
    }
    
    func addPrimaryShadow() {
        
        self.layer.shadowColor = UIColor.black.withAlphaComponent(0.1).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 1.0
    }
    
    func setTransformFullBottom() {
        self.transform = CGAffineTransform(translationX: 0, y: self.bounds.maxY)
    }
    
    func setTransformFullTop() {
        self.transform = CGAffineTransform(translationX: 0, y: -self.bounds.maxY)
    }
    
    func setTransformIdentity() {
        self.transform = CGAffineTransform.identity
    }
    
    func constrainToEdges(_ subview: UIView) {
        
        subview.translatesAutoresizingMaskIntoConstraints = false
        
        let topContraint = NSLayoutConstraint(
            item: subview,
            attribute: .top,
            relatedBy: .equal,
            toItem: self,
            attribute: .top,
            multiplier: 1.0,
            constant: 0)
        
        let bottomConstraint = NSLayoutConstraint(
            item: subview,
            attribute: .bottom,
            relatedBy: .equal,
            toItem: self,
            attribute: .bottom,
            multiplier: 1.0,
            constant: 0)
        
        let leadingContraint = NSLayoutConstraint(
            item: subview,
            attribute: .leading,
            relatedBy: .equal,
            toItem: self,
            attribute: .leading,
            multiplier: 1.0,
            constant: 0)
        
        let trailingContraint = NSLayoutConstraint(
            item: subview,
            attribute: .trailing,
            relatedBy: .equal,
            toItem: self,
            attribute: .trailing,
            multiplier: 1.0,
            constant: 0)
        
        addConstraints([
            topContraint,
            bottomConstraint,
            leadingContraint,
            trailingContraint])
    }
}

extension UITextView {
    
    func trim() -> UITextView {
        self.text = self.text?.trim()
        return self
    }
    
    var isEmpty: Bool {
        return (self.text ?? "").isEmpty
    }
}

extension UITextField {
    
    func trim() -> UITextField {
        self.text = self.text?.trim()
        return self
    }
    
    var isEmpty: Bool {
        return (self.text ?? "").isEmpty
    }
}
