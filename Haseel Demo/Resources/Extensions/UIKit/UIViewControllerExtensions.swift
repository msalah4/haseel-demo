//
//  UIViewControllerExtensions.swift
//  GDP
//
//  Created by Michelle Gerges on 9/14/17.
//  Copyright © 2017 Michelle Gerges. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func embedViewController(_ vc: UIViewController, to containerView: UIView) {
        
        if vc.parent == self {
            return
        }
        
        vc.willMove(toParent: self)
        
        // Add to containerview
        containerView.addSubview(vc.view)
        self.addChild(vc)
        
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            vc.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            vc.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            vc.view.topAnchor.constraint(equalTo: containerView.topAnchor),
            vc.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
        ])
        
        vc.didMove(toParent: self)
    }
    
    func erasViewController(_ vc: UIViewController) {
        
        if vc.parent != self {
            return
        }
        
        vc.willMove(toParent: nil)
        vc.view.removeFromSuperview()
        vc.removeFromParent()
    }
    
    func erasFirstViewController() {
        
        let vc = self.children.first
        
        vc?.willMove(toParent: nil)
        vc?.view.removeFromSuperview()
        vc?.removeFromParent()
    }
    
    func showOrPresent(_ vc: UIViewController) {
        
        if self is UINavigationController {
            (self as? UINavigationController)?.show(vc, sender: self)
            return
        }
        
        if self.navigationController != nil {
            self.navigationController?.show(vc, sender: self)
        } else {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    class func xibName() -> String {
        return String(describing: self)
    }
    
    var previousViewController: UIViewController? {
        
        guard let prevVCs = self.navigationController?.viewControllers else {
            return nil
        }
        
        return prevVCs.count >= 2 ? prevVCs[prevVCs.count - 2] : nil
    }
    
    func setNavigationTitle(_ title: String?) {
        self.navigationItem.title = title
    }
    
    static func loadFromNib() -> Self {
        func instantiateFromNib<T: UIViewController>() -> T {
            return T (nibName: String(describing: T.self), bundle: nil)
        }

        return instantiateFromNib()
    }
}
