//
//  UITableView+Animation.swift
//  Customer IOS
//
//  Created by Michele on 12/4/19.
//  Copyright © 2019 Michelle. All rights reserved.
//

import UIKit

extension UITableView {
    
    func reloadSectionIfExist(section: Int) {
        if self.numberOfSections > section {
            self.reloadSections([section], with: .fade)
        } else {
            self.reloadData()
        }
    }
    
    func scrollToCell(_ cell: UITableViewCell?) {
        
        if let cell = cell, let indexPath = self.indexPath(for: cell) {
            self.scrollToRowIfExist(at: indexPath)
        }
    }
    
    func scrollToRowIfExist(at indexPath: IndexPath) {
        
        if self.numberOfSections > indexPath.section
            && self.numberOfRows(inSection: indexPath.section) > indexPath.row {
            self.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
    func reloadDataAnimated (completion: ((Bool) -> Void)? = nil) {
        UIView.transition(with: self,
                          duration: 0.5,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.reloadData()
        },
                          completion: completion)
    }
    
    func reloadData( completion: (() -> Void)?) {
        
        UIView.animate(withDuration: 0.0, animations: {
            self.reloadData()
        }, completion: { _ in
            completion?()
        })
    }
    
    func setHeaderViewAnimated(_ headerView: UIView) {
        
        if self.tableHeaderView == headerView {
            return
        }
        
        headerView.isHidden = true
        self.tableHeaderView = UIView()
        
        self.beginUpdates()
        UIView.animate(withDuration: 0.25, animations: {
            self.tableHeaderView?.frame = headerView.frame
        }, completion: { (_) in
            self.tableHeaderView = headerView
            self.tableHeaderView?.showupAnimated()
        })
        self.endUpdates()
    }
    
    func setFooterViewAnimated(_ footerView: UIView) {
        
        if self.tableFooterView == footerView {
            return
        }
        
        footerView.isHidden = true
        self.tableFooterView = UIView()
        
        self.beginUpdates()
        UIView.animate(withDuration: 0.25, animations: {
            self.tableFooterView?.frame = footerView.frame
        }, completion: { (_) in
            self.tableFooterView = footerView
            self.tableFooterView?.showupAnimated()
        })
        self.endUpdates()
    }
}
