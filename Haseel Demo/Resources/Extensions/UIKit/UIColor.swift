//
//  UIColor.swift
//  Customer IOS
//
//  Created by michelle gergs on 11/4/20.
//  Copyright © 2020 The Chefz. All rights reserved.
//

import UIKit

extension UIColor {

  @nonobjc class var darkMint: UIColor {
    return UIColor(red: 72.0 / 255.0, green: 185.0 / 255.0, blue: 118.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var black: UIColor {
    return UIColor(white: 0.0, alpha: 1.0)
  }

  @nonobjc class var iceBlue: UIColor {
    return UIColor(red: 241.0 / 255.0, green: 242.0 / 255.0, blue: 243.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var veryLightPink: UIColor {
    return UIColor(white: 235.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var lightishRed: UIColor {
    return UIColor(red: 248.0 / 255.0, green: 54.0 / 255.0, blue: 66.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var veryLightPinkTwo: UIColor {
    return UIColor(white: 227.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var brownGrey: UIColor {
    return UIColor(white: 158.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var brownGreyTwo: UIColor {
    return UIColor(white: 125.0 / 255.0, alpha: 1.0)
  }
    
    @nonobjc class var blueyGrey: UIColor {
        return UIColor(red: 157.0 / 255.0, green: 164.0 / 255.0, blue: 175.0 / 255.0, alpha: 1.0)
      }

}
