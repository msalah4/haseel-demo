//
//  Bundle+Extension.swift
//  Coach You
//
//  Created by Mohammed Salah on 05/05/2020.
//  Copyright © 2020 msalah. All rights reserved.
//

import UIKit
import L10n_swift

private var associatedLanguageBundle:Character = "0"

class PrivateBundle: Bundle {
    static var onceToken: Int = 0
    override func localizedString(forKey key: String, value: String?, table tableName: String?) -> String {
        let bundle: Bundle? = objc_getAssociatedObject(self, &associatedLanguageBundle) as? Bundle
        var extraB = Bundle(path: Bundle.main.path(forResource: L10n.shared.language, ofType: "lproj") ?? "")
        if extraB ==  nil {
            extraB = Bundle(path: Bundle.main.path(forResource: "Base", ofType: "lproj") ?? "")
        }
        return (bundle != nil) ? (bundle!.localizedString(forKey: key, value: value, table: tableName)) : (extraB!.localizedString(forKey: key, value: value, table: tableName))

    }
}

extension Bundle {
    class func setLanguage(_ language: String) {
        if (PrivateBundle.onceToken == 0) {
            /* TODO: move below code to a static variable initializer (dispatch_once is deprecated) */
            object_setClass(Bundle.main, PrivateBundle.self)
        }
        PrivateBundle.onceToken = 1
        if language == "en" {
            objc_setAssociatedObject(Bundle.main, &associatedLanguageBundle, Bundle(path: Bundle.main.path(forResource: "Base", ofType: "lproj") ?? ""), .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        } else {
            objc_setAssociatedObject(Bundle.main, &associatedLanguageBundle, Bundle(path: Bundle.main.path(forResource: language, ofType: "lproj") ?? ""), .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        
        
        let isRTL  = (Locale.characterDirection(forLanguage: language) == .rightToLeft)
        if isRTL {
            
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UITextField.appearance().semanticContentAttribute = .forceRightToLeft
        } else {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            UITextField.appearance().semanticContentAttribute = .forceLeftToRight
        }
        UserDefaults.standard.set(language, forKey: "currentLanguageKey")
//        UserDefaults.standard.set([L10n.shared.language], forKey: "AppleLanguages")
        UserDefaults.standard.set(isRTL, forKey: "AppleTextDirection")
        UserDefaults.standard.set(isRTL, forKey: "NSForceRightToLeftWritingDirection")
        UserDefaults.standard.synchronize()
    }
}
