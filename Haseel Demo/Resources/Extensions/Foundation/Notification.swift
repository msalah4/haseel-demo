//
//  Notification.swift
//  Customer IOS
//
//  Created by Michele on 12/8/19.
//  Copyright © 2019 Michelle. All rights reserved.
//

import Foundation

extension NSNotification.Name {
    static let UserDidLogin = NSNotification.Name("UserDidLogin")
}
