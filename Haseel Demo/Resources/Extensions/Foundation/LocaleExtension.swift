//
//  LocaleExtension.swift
//  Customer IOS
//
//  Created by Michele on 12/12/19.
//  Copyright © 2019 Michelle. All rights reserved.
//

import Foundation

extension Locale {
    static var arabicLocale: Locale {
        return Locale(identifier: "ar_EH")
    }
    
    static var englishLocale: Locale {
        return Locale(identifier: "en")
    }
    
    static var userSelectedLocale: Locale {
        return LocalizeService.shared.isArabic ? arabicLocale : englishLocale
    }
}
