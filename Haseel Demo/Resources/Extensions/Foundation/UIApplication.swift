//
//  UIApplication.swift
//  AJEL
//
//  Created by Michelle Gerges on 7/31/18.
//  Copyright © 2018 Michelle Gerges. All rights reserved.
//

import Foundation
import UIKit
import IQKeyboardManagerSwift
import MapKit

extension UIApplication {
    
    var topViewController: UIViewController? {
        
        let window = (self.delegate as? AppDelegate)?.window
        let rootNaviationController = window?.rootViewController as? UINavigationController
        return rootNaviationController?.topViewController ?? window?.rootViewController
    }
    
    var window: UIWindow? {
        return (self.delegate as? AppDelegate)?.window
    }
    
    func restartTo(_ viewController: UIViewController) {
        
        if let window = self.window {
            
            UIView.transition(with: window,
                              duration: 0.5,
                              options: .transitionCrossDissolve,
                              animations: {
                                window.rootViewController = viewController
                                
            },
                              completion: nil)
        }
    }
    
    func openURLWithSafariBrowser(_ urlString: String?) {
        
        if let urlString = urlString,
            let url = URL(string: urlString) {
            self.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func callA(number: String?) {
        
        if let numberItSelf = number,
            let url = URL(string: "tel://\(numberItSelf)") {
            self.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func sendMail(email: String?) {
        
        if let emailItSelf = email,
            let url = URL(string: "mailto://\(emailItSelf)") {
            self.open(url, options: [:], completionHandler: nil)
        }
    }
    
    /*
     ### It Opens Map Application ###
     
     it check if the device has google Maps app
     if not then it opens apple default maps app
     
     - Parameter latitude: coodrinate
     - Parameter longitude: coordinate
     - Returns: Void.
     */
    
    func openMapsApp(latitude: Double?, longitude: Double?, name: String? = nil) {
        
        guard let lat = latitude, let long = longitude else { return }
        
        guard let googleMapsAppSchemaURL = URL(string: "comgooglemaps://"),
            let googleMapsAppURL = URL(string: "comgooglemaps://?center=\(lat),\(long)&q=\(lat),\(long)"),
            let appleMapsAppSchemaURL = URL(string: "http://maps.apple.com"),
            let googleMapsWepsitURL = URL(string: "https://www.google.com/maps/@\(lat),\(long)") else {
                return
        }
        
        if self.canOpenURL(googleMapsAppSchemaURL) {
            self.open(googleMapsAppURL, options: [:], completionHandler: nil)
            return
        }
        
        if self.canOpenURL(appleMapsAppSchemaURL) {
            
            let coordinates = CLLocationCoordinate2DMake(lat, long)
            let placemark = MKPlacemark(coordinate: coordinates)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = name
            mapItem.openInMaps(launchOptions: nil)
            
            return
        }
        
        self.open(googleMapsWepsitURL, options: [:], completionHandler: nil)
    }
    
    func goToAppSettings() {
        
        if let locationSettingsUrl = URL(string: UIApplication.openSettingsURLString) {
            self.open(locationSettingsUrl, options: [:], completionHandler: nil)
        }
    }
}
