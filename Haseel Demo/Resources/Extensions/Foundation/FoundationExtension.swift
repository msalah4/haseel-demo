//
//  FoundationsExtension.swift
//  Jitensha
//
//  Created by Michelle Gergs on 5/21/17.
//  Copyright © 2017 michelleGergs. All rights reserved.
//

import Foundation

extension Dictionary {

    func removeNullValues() -> Dictionary {
        return self.filter({ (arg0) -> Bool in
            let (_, value) = arg0
            return !(value is NSNull)
        })
    }
    
    static func + (lhs: Dictionary, rhs: Dictionary) -> Dictionary {
        
        var merged = lhs
        
        rhs.forEach {
            merged[$0] = $1
        }
        return merged
    }
}

extension UInt32 {
    
    init?(hexString: String) {

        let scanner = Scanner(string: hexString)
        var hexInt = UInt32.max
        let success = scanner.scanHexInt32(&hexInt)
        if success {
            self = hexInt
        } else {
            return nil
        }
    }
}

extension Data {

    func sizeInKB() -> Int {
        return NSData(data: self).length / 1024 // in Kilo
    }

    func toBase64String(imageExtesion: String) -> String {
        return imageExtesion + self.base64EncodedString(options: Data.Base64EncodingOptions.endLineWithLineFeed)
    }
}

extension Double {

    var distanceFormat: String {
        
        let thounds = self / 1000
        
        if thounds >= 1 {
            return "\((thounds * 10).rounded() / 10) KM"
        } else {
            return "\((self * 10).rounded() / 10) M"
        }
    }
}
