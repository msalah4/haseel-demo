//
//  DateExtension.swift
//  AJEL
//
//  Created by Michelle Gerges on 5/19/18.
//  Copyright © 2018 Michelle Gerges. All rights reserved.
//

import Foundation

extension Date {

    static var dateFormater: DateFormatter = {
        return DateFormatter()
    }()

    static var islamicCalendar: Calendar = {

        let calendar: Calendar = Calendar(identifier: .islamicUmmAlQura)

        return calendar
    }()

    static var gregorianCalendar: Calendar = {

        let calendar: Calendar = Calendar(identifier: .gregorian)

        return calendar
    }()

    // ------------------------------------------------------------------------------

    static func date( milliSeconds: Double?) -> Date? {

        guard let milliSeconds = milliSeconds else { return nil }

        return Date(timeIntervalSince1970: TimeInterval( milliSeconds / 1000) )
    }

    static func date( seconds: Double?) -> Date? {

        guard let seconds = seconds else { return nil }

        return Date(timeIntervalSince1970: TimeInterval( seconds) )
    }

    static func date(gmtDateString: String, inFormat formatString: String) -> Date? {

        let dateFormater = Self.dateFormater
        
        dateFormater.dateFormat = formatString
        dateFormater.locale = Locale(identifier: "en")
        dateFormater.calendar = Date.gregorianCalendar

        return dateFormater.date(from: gmtDateString)
    }
    
    static func date(ksaDateString: String, inFormat formatString: String) -> Date? {

        let dateFormater = Self.dateFormater
        
        dateFormater.dateFormat = formatString
        dateFormater.locale = Locale(identifier: "en")
        dateFormater.calendar = Date.gregorianCalendar

        return dateFormater.date(from: ksaDateString)
    }

    func toMilliSeconds () -> Double {
        return self.timeIntervalSince1970 * 1000
    }

    func toSeconds () -> Double {
        return self.timeIntervalSince1970
    }
    
    func toSecondsDateOnly () -> Double {
        
        let dateMilliSeconds = self.toSeconds()
        
        let dayTimeInSecondes = Double(24 * 60 * 60)
        
        return dateMilliSeconds - (dateMilliSeconds.truncatingRemainder(dividingBy: dayTimeInSecondes))
    }
    // ------------------------------------------------------------------------------

    func toLocalizedString(_ formate: String) -> String {

        if LocalizeService.shared.isRTL {
            return toArbicString(formate)
        } else {
            return toString(formate)
        }
    }

    func toLocalizedIslamicString(_ formate: String) -> String {

        if LocalizeService.shared.isRTL {
            return toArbicIslamicString(formate)
        } else {
            return toIslamicString(formate)
        }
    }

    func toString(_ formate: String) -> String {

        Date.dateFormater.dateFormat = formate

        Date.dateFormater.locale = Locale(identifier: "en")
        Date.dateFormater.calendar = Date.gregorianCalendar

        return Date.dateFormater.string(from: self)
    }

    func toArbicString(_ formate: String) -> String {

        Date.dateFormater.dateFormat = formate

        Date.dateFormater.locale = Locale(identifier: "ar_EH")
        Date.dateFormater.calendar = Date.gregorianCalendar

        return Date.dateFormater.string(from: self)
    }
    // --------------------------------------------------
    func toIslamicString(_ formate: String) -> String {

        Date.dateFormater.dateFormat = formate

        Date.dateFormater.locale = Locale(identifier: "EH")
        Date.dateFormater.calendar = Date.islamicCalendar

        return Date.dateFormater.string(from: self)
    }

    func toArbicIslamicString(_ formate: String) -> String {

        Date.dateFormater.dateFormat = formate

        Date.dateFormater.locale = Locale(identifier: "ar_EH")
        Date.dateFormater.calendar = Date.islamicCalendar

        return Date.dateFormater.string(from: self)
    }
  
  func dateFormatTimeToString() -> String {
         let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = "dd/MM/yyyy"
         dateFormatter.locale = Locale.userSelectedLocale
         return dateFormatter.string(from: self)
     }
    
}
