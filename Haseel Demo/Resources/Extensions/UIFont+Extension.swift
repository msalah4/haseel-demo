//
//  UIFont+Extension.swift
//  Fly365
//
//  Created by Mohammed Salah on 11/07/2019.
//  Copyright © 2019 Fly365. All rights reserved.
//

import UIKit
import L10n_swift

struct AppFontName {
    //ReemKufi-Regular.ttf,GESSTextLight-Light
    static var regular: String {
        get {
           ((L10n.shared.language == "ar") ? "FF Shamel Family Unique Book" : "Poppins-Regular")
        }
    }
    
    static var bold: String  {
        get {
            ((L10n.shared.language == "ar") ? "FF Shamel Family Unique Bold" : "Poppins-Bold")
        }
    }
    
    static var italic: String {
        get {
            "Poppins-LightItalic"
        }
    }
    static var semiBold: String {
        get {
            ((L10n.shared.language == "ar") ? "FF Shamel Family Unique Medium" : "Poppins-SemiBold")
        }
    }
    static var medium: String {
        get {
            ((L10n.shared.language == "ar") ? "FF Shamel Family Unique Medium" : "Poppins-Medium")
        }
    }
}

extension UIFontDescriptor.AttributeName {
    static let nsctFontUIUsage = UIFontDescriptor.AttributeName(rawValue: "NSCTFontUIUsageAttribute")
}

extension UIFont {
    
    var weight: UIFont.Weight {
        guard let weightNumber = traits[.weight] as? NSNumber else { return .regular }
        let weightRawValue = CGFloat(weightNumber.doubleValue)
        let weight = UIFont.Weight(rawValue: weightRawValue)
        return weight
    }
    
    private var traits: [UIFontDescriptor.TraitKey: Any] {
        return fontDescriptor.object(forKey: .traits) as? [UIFontDescriptor.TraitKey: Any]
            ?? [:]
    }


    
    @objc class func myRegularSystemFont(ofSize size: CGFloat) -> UIFont {
        print(L10n.shared.language)
        return UIFont(name: AppFontName.regular, size: size)!
    }

    @objc class func myBoldSystemFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: AppFontName.bold, size: size) ?? UIFont(name: AppFontName.semiBold, size: size)!
    }

    @objc class func myItalicSystemFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: AppFontName.italic, size: size)!
    }

    @objc class func mySemiBoldSystemFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: AppFontName.semiBold, size: size)!
    }
    
    @objc class func mySemiMediumSystemFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: AppFontName.medium, size: size)!
    }
    
//    @objc class func myInitSystemFont(name: String, size: CGFloat) -> UIFont {
//        let font = MyFont.init()
//        font.fontfName = AppFontName.regular
//        font.pointfSize = size
////        font.fontName =
////        font.pointSize = size
//        return font//UIFont(name: AppFontName.regular, size: size)!
//    }

    @objc class func
        mySystemFontWithWeight(ofSize size: CGFloat, weight: UIFont.Weight) -> UIFont {
        if weight == .semibold {
            return UIFont(name: AppFontName.semiBold, size: size)!
        } else if weight == .bold {
            return UIFont(name: AppFontName.bold, size: size) ?? UIFont(name: AppFontName.semiBold, size: size)!
        } else if weight == .medium {
                   return UIFont(name: AppFontName.medium, size: size) ?? UIFont(name: AppFontName.semiBold, size: size)!
               } else {
            return UIFont(name: AppFontName.regular, size: size)!
        }
    }

    @objc convenience init(myCoder aDecoder: NSCoder) {
        guard
            let fontDescriptor = aDecoder.decodeObject(forKey: "UIFontDescriptor") as? UIFontDescriptor,
            let fontAttribute = fontDescriptor.fontAttributes[.nsctFontUIUsage] as? String else {
            self.init(myCoder: aDecoder)
            return
        }
        var fontName = ""
        switch fontAttribute {
        case "CTFontRegularUsage":
            fontName = AppFontName.regular
        case "CTFontEmphasizedUsage", "CTFontBoldUsage":
            fontName = AppFontName.bold
        case "CTFontObliqueUsage":
            fontName = AppFontName.italic
        case "CTFontDemiUsage":
            fontName = AppFontName.medium
        default:
            fontName = AppFontName.regular
        }
        self.init(name: fontName, size: fontDescriptor.pointSize)!
    }

    class func overrideInitialize() {
        guard self == UIFont.self else { return }

        if let systemFontMethod = class_getClassMethod(self, #selector(systemFont(ofSize:))),
            let mySystemFontMethod = class_getClassMethod(self, #selector(myRegularSystemFont(ofSize:))) {
            method_exchangeImplementations(systemFontMethod, mySystemFontMethod)
        }

        if let boldSystemFontMethod = class_getClassMethod(self, #selector(boldSystemFont(ofSize:))),
            let myBoldSystemFontMethod = class_getClassMethod(self, #selector(myBoldSystemFont(ofSize:))) {
            method_exchangeImplementations(boldSystemFontMethod, myBoldSystemFontMethod)
        }

        if let italicSystemFontMethod = class_getClassMethod(self, #selector(italicSystemFont(ofSize:))),
            let myItalicSystemFontMethod = class_getClassMethod(self, #selector(myItalicSystemFont(ofSize:))) {
            method_exchangeImplementations(italicSystemFontMethod, myItalicSystemFontMethod)
        }

        if let semiBoldSystemFontMethod = class_getClassMethod(self, #selector(systemFont(ofSize:weight:))),
            let mySemiBoldSystemFontMethod = class_getClassMethod(self, #selector(mySystemFontWithWeight(ofSize:weight:))) {
            
            method_exchangeImplementations(semiBoldSystemFontMethod, mySemiBoldSystemFontMethod)
        }
        
//        if let initSystemFontMethod = class_getClassMethod(self, #selector(UIFont.init(name:size:))),
//            let myInitSystemFontMETHOD = class_getClassMethod(self, #selector(myInitSystemFont(name:size:))) {
//
//            method_exchangeImplementations(initSystemFontMethod, myInitSystemFontMETHOD)
//        }
//        
        

        if let initCoderMethod = class_getInstanceMethod(self, #selector(UIFontDescriptor.init(coder:))), // Trick to get over the lack of UIFont.init(coder:))
            let myInitCoderMethod = class_getInstanceMethod(self, #selector(UIFont.init(myCoder:))) {
            method_exchangeImplementations(initCoderMethod, myInitCoderMethod)
        }
    }
}


extension UILabel {
    @objc var substituteFontName : String {
        get {
            return self.font.fontName;
        }
        set {
            let fontNameToTest = self.font.fontName.lowercased();
            var fontName = newValue;
            if fontNameToTest.range(of: "bold") != nil {
                fontName = AppFontName.bold;
            } else if fontNameToTest.range(of: "medium") != nil {
                fontName = AppFontName.medium;
            } else if fontNameToTest.range(of: "light") != nil {
                fontName = AppFontName.regular;
            } else if fontNameToTest.range(of: "semi") != nil {
                fontName = AppFontName.semiBold;
            } else {
                fontName = AppFontName.regular;
            }
            self.font = UIFont(name: fontName, size: self.font.pointSize)
        }
    }
}

extension UITextView {
    @objc var substituteFontName : String {
        get {
            return self.font?.fontName ?? "";
        }
        set {
            let fontNameToTest = self.font?.fontName.lowercased() ?? "";
            var fontName = newValue;
            if fontNameToTest.range(of: "bold") != nil {
                fontName = AppFontName.bold;
            } else if fontNameToTest.range(of: "medium") != nil {
                fontName = AppFontName.medium;
            } else if fontNameToTest.range(of: "light") != nil {
                fontName = AppFontName.regular;
            } else if fontNameToTest.range(of: "semi") != nil {
                fontName = AppFontName.semiBold;
            } else {
                fontName = AppFontName.regular;
            }
            self.font = UIFont(name: fontName, size: self.font?.pointSize ?? 17)
        }
    }
}

extension UITextField {
    @objc var substituteFontName : String {
        get {
            return self.font?.fontName ?? "";
        }
        set {
            let fontNameToTest = self.font?.fontName.lowercased() ?? "";
            var fontName = newValue;
            if fontNameToTest.range(of: "bold") != nil {
                fontName = AppFontName.bold;
            } else if fontNameToTest.range(of: "medium") != nil {
                fontName = AppFontName.medium;
            } else if fontNameToTest.range(of: "light") != nil {
                fontName = AppFontName.regular;
            } else if fontNameToTest.range(of: "semi") != nil {
                fontName = AppFontName.semiBold;
            } else {
                fontName = AppFontName.regular;
            }
            self.font = UIFont(name: fontName, size: self.font?.pointSize ?? 17)
        }
    }
}
