//
//  CartViewModel.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 02/12/2020.
//

import UIKit

class CartViewModel: NSObject {
    
    var selectedPaymentMethod: PaymentMethodType? = nil
    let addressDataSource = LocalSourceCartDataSource()
    let deliveryTimeDataSource = LocalDeliveryTimeDataSource()
    let cartItemDataSource = LocalCartItemDataSource()
    var selectedAddressIndex = 0
    var selectedDayIndex = -1
    var selectedSlotIndex = -1
    var promoValue = ""
    
    override init() {
        super.init()
        selectedAddressIndex = self.defaultAddressIndex()
    }
    
    func getAllSavedAddress () -> [Address] {
        return addressDataSource.fetchAllAddress() ?? []
    }
    
    func getDeliveryTime () -> DeliveryTime? {
        return deliveryTimeDataSource.fetchAllDeliveryTime()
    }
    
    func getCartItems () -> [CartItem] {
        return cartItemDataSource.fetchAllItems() ?? []
    }
    
    func defaultAddressIndex() -> Int {
        let addresses = getAllSavedAddress()
        if addresses.isEmpty {
            return 0
        }
        var index = 0
        for address in addresses {
            if address.defaultAddress ?? false {
                return index
            }
            index += 1
        }
        
        return 0
    }
 }
