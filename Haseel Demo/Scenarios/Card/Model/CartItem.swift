//
//  CartItem.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 03/12/2020.
//

import UIKit

class CartItem: Codable {
    let imageName, title, desc, price: String?
    let currency: String?
}
