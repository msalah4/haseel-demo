//
//  DeliveryTime.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 03/12/2020.
//

import UIKit

// MARK: - DeliveryTime
struct DeliveryTime: Codable {
    let deliveryDays: [DeliveryDay]?
    let slots: [String]?

    enum CodingKeys: String, CodingKey {
        case deliveryDays = "DeliveryDays"
        case slots
    }
}

// MARK: - DeliveryDay
struct DeliveryDay: Codable {
    let dayNameAr, dayWeekAr, dayNameEn, dayWeekEn: String?
    
    func getDayName() -> String {
        return dayNameEn ?? ""
    }
    
    func getDayWeek() -> String {
        return dayWeekEn ?? ""
    }
}
