//
//  Address.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 01/12/2020.
//

import UIKit

class Address: Codable {
    
    let type, desc: String?
    let defaultAddress: Bool?
    
    enum CodingKeys: String, CodingKey {
        case type, desc
        case defaultAddress = "default"
    }
}
