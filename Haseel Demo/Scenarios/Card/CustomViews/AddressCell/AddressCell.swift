//
//  AddressCell.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 29/11/2020.
//

import UIKit

class AddressCell: UITableViewCell {
    
    @IBOutlet weak var addressLabel: UILabel!
    var didPressChangeAddress: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func didPressChange(_ sender: Any) {
        if let update = didPressChangeAddress {
            update()
        }
    }
}
