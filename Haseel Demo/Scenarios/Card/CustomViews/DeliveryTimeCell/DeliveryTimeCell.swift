//
//  DeliveryTimeCell.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 01/12/2020.
//

import UIKit

class DeliveryTimeCell: UITableViewCell {
    
    @IBOutlet weak var DaysCollectionView: UICollectionView!
    @IBOutlet weak var slotsCollectionView: UICollectionView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
