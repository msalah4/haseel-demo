//
//  DeliveryTimeDaysCell.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 03/12/2020.
//

import UIKit

class DeliveryTimeDaysCell: UICollectionViewCell {

    @IBOutlet weak var ContainerView: UIView!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dayWeekLabel: UILabel!
    
    var textColor: UIColor = .black
    var isSelectedMethod: Bool = false {
        didSet {
            reloadCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        isSelectedMethod = false
    }
    
    func reloadCell () {
      
        if !isSelectedMethod {
            self.ContainerView.borderWidth = 0
            textColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.6)
            ContainerView.backgroundColor = .iceBlue
        } else {
            self.ContainerView.borderWidth = 2
            textColor = .black
            ContainerView.backgroundColor = .white
        }
        dayLabel.textColor = textColor
        dayWeekLabel.textColor = textColor
    }

}
