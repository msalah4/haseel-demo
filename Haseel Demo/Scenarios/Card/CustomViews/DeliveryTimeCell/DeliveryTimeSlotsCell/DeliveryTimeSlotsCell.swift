//
//  DeliveryTimeSlotsCell.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 03/12/2020.
//

import UIKit

class DeliveryTimeSlotsCell: UICollectionViewCell {
    
    @IBOutlet weak var ContainerView: UIView!
    @IBOutlet weak var slotLabel: UILabel!
    
    var textColor: UIColor = .black
    var isSelectedMethod: Bool = false {
        didSet {
            reloadCell()
        }
    }
    
    override func awakeFromNib() {
        isSelectedMethod = false
    }

    func reloadCell () {
      
        if !isSelectedMethod {
            self.ContainerView.borderWidth = 0
            textColor = UIColor.blueyGrey
            ContainerView.backgroundColor = .iceBlue
        } else {
            self.ContainerView.borderWidth = 2
            textColor = .white
            ContainerView.backgroundColor = .darkMint
        }
        slotLabel.textColor = textColor
    }

}
