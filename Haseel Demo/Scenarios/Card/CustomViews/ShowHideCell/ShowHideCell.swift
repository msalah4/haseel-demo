//
//  ShowHideCell.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 01/12/2020.
//

import UIKit

class ShowHideCell: UITableViewCell {

    @IBOutlet weak var detailsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
