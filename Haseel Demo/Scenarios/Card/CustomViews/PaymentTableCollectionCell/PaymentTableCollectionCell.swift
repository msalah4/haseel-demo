//
//  TableCollectionCell.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 30/11/2020.
//

import UIKit
import Stellar

enum PaymentMethodType: String {
    case applePay = "applePay"
    case stcPay = "stc"
    case visa = "visa"
    case cash = "cash"
    case network = "machine"
    
    static func allItems() ->[PaymentMethodType] {
        return [.applePay, .stcPay, .visa, .cash, .network]
    }
}

class PaymentTableCollectionCell: UITableViewCell {
    
    @IBOutlet weak var externalView: UIView!
    @IBOutlet weak var externalViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var containerHeightConstraint: NSLayoutConstraint!
    let externalViewHeight: CGFloat = 50
    var showExtraInfo = false {
        didSet {
            updateExtraInfo()
        }
    }
    var didUpdateContent: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //        externalViewHeightConstraint.constant = 0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateExtraInfo() {
        
        if showExtraInfo {
            UIView.animate(withDuration: 0.5) {
                //                self.externalViewHeightConstraint.constant = 0
                self.containerHeightConstraint.constant = 170
            }
        } else {
            UIView.animate(withDuration: 0.5) {
                //                self.externalViewHeightConstraint.constant = self.externalViewHeight
                self.containerHeightConstraint.constant = 118
            }
        }
        
        self.didUpdateContent!()
    }
    
}
