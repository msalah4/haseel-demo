//
//  PaymentMethodCell.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 01/12/2020.
//

import UIKit



class PaymentMethodCell: UICollectionViewCell {

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var ContainerView: UIView!
    var extraImageName = ""
    var textColor: UIColor = .black
    var isSelectedMethod: Bool = false {
        didSet {
            reloadCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func reloadCell () {
        if stackView == nil {
            return
        }
        stackView.removeAllArrangedSubviews()
        if !isSelectedMethod {
            self.ContainerView.borderWidth = 0
            extraImageName = ""
            textColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.6)
            ContainerView.backgroundColor = .iceBlue
        } else {
            self.ContainerView.borderWidth = 2
            extraImageName = "_selected"
            textColor = .black
            ContainerView.backgroundColor = .white
        }
        
    }
    
    
    func addImageWithName( name: String) {
        let image = UIImageView(image: UIImage(named: name + extraImageName))
        image.contentMode = .center
        stackView.addArrangedSubview(image)
    }
    
    func addLabelWithName  (name: String) {
        let label = UILabel()
        label.text = name
        label.font = UIFont.systemFont(ofSize: 12.0)
        label.textColor = textColor
        stackView.addArrangedSubview(label)
    }
    

}
