//
//  CouponCell.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 29/11/2020.
//

import UIKit
import Stellar
class CouponCell: UITableViewCell {

    @IBOutlet weak var topConstraintTitleCouponLabel: NSLayoutConstraint!
    @IBOutlet weak var couponTitleLabel: UILabel!
    @IBOutlet weak var couponValueLabel: UILabel!
    @IBOutlet weak var addPromoButton: UIButton!
    var couponValue = "" {
        didSet {
            didUpdateCouponValue()
        }
    }
    var didPressChangeCoupon: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func didUpdateCouponValue () {
        if couponValue.isEmpty {
            topConstraintTitleCouponLabel.constant = 23
            addPromoButton.setImage(UIImage(named: "addCirlce"), for: .normal)
            addPromoButton.setTitle("", for: .normal)
        } else {
            topConstraintTitleCouponLabel.constant = 14
            addPromoButton.setImage(nil , for: .normal)
            addPromoButton.setTitle("Change".l10n(), for: .normal)
        }
        self.layoutIfNeeded()
//        couponTitleLabel.moveTo(currentPoint).moveX(49).animate()
        couponValueLabel.text = couponValue
    }
    
    @IBAction func didPressChange(_ sender: Any) {
        if let update = didPressChangeCoupon {
            update()
        }
    }
    
}
