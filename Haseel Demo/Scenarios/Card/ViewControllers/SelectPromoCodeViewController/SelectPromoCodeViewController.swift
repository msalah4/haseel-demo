//
//  SelectPromoCodeViewController.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 02/12/2020.
//

import UIKit
import TransitionButton

protocol SelectPromoCodeDelegate: class {
    func didAddPromoCode (promo: String)
}

class SelectPromoCodeViewController: UIViewController {
    
    @IBOutlet weak var promoTextField: UITextField!
    @IBOutlet weak var addButton: TransitionButton!
    weak var delegate: SelectPromoCodeDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

    @IBAction func didPressDismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didPressAddPromo(_ sender: Any) {
        
        addButton.startAnimation()
        
        _ = Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false) { [unowned self] timer in
            self.addButton.stopAnimation()
            if let del = self.delegate {
                del.didAddPromoCode(promo: self.promoTextField.text ?? "")
            }
            dismiss(animated: true, completion: nil)
        }
        
    }
}
