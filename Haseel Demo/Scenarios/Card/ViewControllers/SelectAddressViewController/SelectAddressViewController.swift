//
//  SelectAddressViewController.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 02/12/2020.
//

import UIKit
import Hero
import Stellar

protocol SelectAddressDelegate: class {
    func didSelecteAddressOfIndex (index :Int)
}

class SelectAddressViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var contentView: UIView!
    
    weak var delegate: SelectAddressDelegate?
    var listOfAddress: [Address] = []
    var selectedAddress = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.makeRoundedCornerForSides(sides: [.topLeft, .topRight], radius: 8)
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.registerNibFor(cellClass: AddressDetailsCell.self)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        contentView.moveY(-contentView.height).duration(0.5).animate()
        headerView.moveY(-contentView.height).duration(0.5).animate()
    }
    
    @IBAction func didPressDismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didPressAddNewAddress(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension SelectAddressViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfAddress.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(cellClass: AddressDetailsCell.self)
        let address = listOfAddress[indexPath.row]
        cell.addressLabel.text = address.desc
        if address.type?.lowercased() == "home" {
            cell.typeLabel.text = "Home".l10n()
            cell.typeImage?.image = UIImage(named: "group")
        } else {
            cell.typeLabel.text = "Work".l10n()
            cell.typeImage?.image = UIImage(named: "suitcase")
        }
        
        cell.isSelectedImage.isHidden = selectedAddress != indexPath.row
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedAddress = indexPath.row
        
        if let del = delegate {
            del.didSelecteAddressOfIndex(index: indexPath.row)
            contentView.moveY(-contentView.height).duration(0.5).animate()
            headerView.moveY(-contentView.height).duration(0.5).animate()
        }
        
        dismiss(animated: true, completion: nil)
    }
    
}
