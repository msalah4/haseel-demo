//
//  CartViewController+CollectionView.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 01/12/2020.
//

import UIKit

extension CartViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == CellType.payment.rawValue {
            return PaymentMethodType.allItems().count
        } else {
            if collectionView.tag == -5 {
                return viewModel.getDeliveryTime()?.deliveryDays?.count ?? 0
            } else if collectionView.tag == -10 {
                return viewModel.getDeliveryTime()?.slots?.count ?? 0
            }
        }
        return 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == CellType.payment.rawValue {
            let cell = collectionView.dequeueReusableCell(cellClass: PaymentMethodCell.self, for: indexPath)
            configurePaymentCell(collectionView, cellForItemAt: indexPath, cell: cell)
            return cell
        } else  {
            if collectionView.tag == -5 {
                let cell = collectionView.dequeueReusableCell(cellClass: DeliveryTimeDaysCell.self, for: indexPath)
                cell.isSelectedMethod = (viewModel.selectedDayIndex >= 0 && viewModel.selectedDayIndex == indexPath.row)
                let day = viewModel.getDeliveryTime()?.deliveryDays?[indexPath.row]
                cell.dayLabel.text = day?.getDayName()
                cell.dayWeekLabel.text = day?.getDayWeek()
                return cell
            } else if collectionView.tag == -10 {
                let cell = collectionView.dequeueReusableCell(cellClass: DeliveryTimeSlotsCell.self, for: indexPath)
                cell.isSelectedMethod = (viewModel.selectedSlotIndex >= 0 && viewModel.selectedSlotIndex == indexPath.row)
                let slot = viewModel.getDeliveryTime()?.slots?[indexPath.row]
                cell.slotLabel.text = slot
                return cell
            }
        }
        
        return UICollectionViewCell()
    }
    
    func configurePaymentCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath, cell: PaymentMethodCell) {
        let methodType = PaymentMethodType.allItems()[indexPath.row]
        cell.isSelectedMethod = (methodType == viewModel.selectedPaymentMethod)
        
        switch methodType {
        case .applePay, .stcPay:
            cell.addImageWithName(name: methodType.rawValue)
        case .visa:
            cell.addImageWithName(name: methodType.rawValue)
            cell.addImageWithName(name: "mada")
        case .cash:
            cell.addImageWithName(name: methodType.rawValue)
            cell.addLabelWithName(name: "Cash")
        case .network:
            cell.addImageWithName(name: methodType.rawValue)
            cell.addLabelWithName(name: "Machine")
        }
        
    }
    
    
}

extension CartViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == CellType.payment.rawValue {
            let methodType = PaymentMethodType.allItems()[indexPath.row]
            viewModel.selectedPaymentMethod = methodType
            if cellsList.contains(.payment) {
                let index = cellsList.firstIndex(of: .payment) ?? 0
                self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
            }
            
            
        } else {
            if collectionView.tag == -5 {
                viewModel.selectedDayIndex = indexPath.row
                if cellsList.contains(.deliveryTime) {
                    let index = cellsList.firstIndex(of: .deliveryTime) ?? 0
                    self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                }
            } else if collectionView.tag == -10 {
                viewModel.selectedSlotIndex = indexPath.row
                if cellsList.contains(.deliveryTime) {
                    let index = cellsList.firstIndex(of: .deliveryTime) ?? 0
                    self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                }
            }
        }
    }
}
