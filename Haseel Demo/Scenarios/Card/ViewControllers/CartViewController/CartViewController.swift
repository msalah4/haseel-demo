//
//  CardViewController.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 29/11/2020.
//

import UIKit
import  L10n_swift
import Hero
enum CellType: Int {
    case address
    case payment
    case balance
    case deliveryTime
    case coupon
    case ItemHeader
    case item
    case showHide
}

class CartViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var orderConfirmationButton: UIButton!
    
    let originalCellsList: [CellType] = [.address, .payment, .balance, .deliveryTime, .coupon, .ItemHeader, .item, .item, .showHide]
    var cellsList: [CellType] = []
    let viewModel = CartViewModel()
    var cartItems: [CartItem] = []
    var isCollappsed = true {
        didSet {
            updateCartItems()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        Hero.shared.containerColor = .black
        cartItems = viewModel.getCartItems()
        cellsList = originalCellsList
        setupTableView()
        
        Bundle.setLanguage(L10n.shared.language)
        
        NotificationCenter.default.addObserver(
            self, selector: #selector(self.didChangeLanguage), name: .L10nLanguageChanged, object: nil
        )
    }

    func setupTableView() {
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.registerNibFor(cellClass: AddressCell.self)
        tableView.registerNibFor(cellClass: WalletCell.self)
        tableView.registerNibFor(cellClass: CouponCell.self)
        tableView.registerNibFor(cellClass: PaymentTableCollectionCell.self)
        tableView.registerNibFor(cellClass: DeliveryTimeCell.self)
        tableView.registerNibFor(cellClass: CartItemCell.self)
        tableView.registerNibFor(cellClass: CartItemHeaderCell.self)
        tableView.registerNibFor(cellClass: ShowHideCell.self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
    }
    
    func showChangeAddressView () {
        
        let selectAddressVC = storyboard?.instantiateViewController(identifier: "SelectAddressViewController") as! SelectAddressViewController
        selectAddressVC.listOfAddress = viewModel.getAllSavedAddress()
        selectAddressVC.selectedAddress = viewModel.selectedAddressIndex
        selectAddressVC.delegate = self
        selectAddressVC.heroModalAnimationType = .selectBy(presenting:.fade, dismissing:.fade)
        selectAddressVC.isHeroEnabled = true
        self.present(selectAddressVC, animated: true, completion: nil)

    }
    
    func showChangeCouponView () {
        let selectCouponVC = storyboard?.instantiateViewController(identifier: "SelectPromoCodeViewController") as! SelectPromoCodeViewController
        selectCouponVC.delegate = self
        selectCouponVC.heroModalAnimationType = .selectBy(presenting:.fade, dismissing:.fade)
        selectCouponVC.isHeroEnabled = true
        self.present(selectCouponVC, animated: true, completion: nil)
    }
    
    func updateCartItems() {
        if isCollappsed {
            cellsList = originalCellsList
        } else {
            let reminingItems = cartItems.suffix(from: 2)
            for _ in reminingItems {
                cellsList.insert(.item, at: cellsList.count - 2)
            }
        }
        tableView.reloadData()
    }
    
    @objc @IBAction func didPressChangeLange( _ sender: Any) {
        if (L10n.shared.language == "ar") {
            L10n.shared.language = "en"
        }else {
            L10n.shared.language = "ar"
        }
    }
    
    @objc func didChangeLanguage() {
        Bundle.setLanguage(L10n.shared.language)
        let splashController = self.storyboard?.instantiateViewController(withIdentifier: "SplashViewController") ?? UIViewController()
        UIApplication.shared.restartTo(splashController)
//        let del = UIApplication.shared.delegate as? AppDelegate//self.view.window!.windowScene!.delegate as? AppDelegate
//        del?.reloadViews()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    

}

extension CartViewController: SelectAddressDelegate {
    func didSelecteAddressOfIndex(index: Int) {
        viewModel.selectedAddressIndex = index
        let index = self.cellsList.firstIndex(of: .address) ?? 0
        self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
    }
}


extension CartViewController: SelectPromoCodeDelegate {
    func didAddPromoCode(promo: String) {
        viewModel.promoValue = promo
        let index = self.cellsList.firstIndex(of: .coupon) ?? 0
        self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
    }
    
    
}
