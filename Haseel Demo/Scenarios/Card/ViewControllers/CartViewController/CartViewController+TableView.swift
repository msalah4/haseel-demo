//
//  CartViewController+TableView.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 29/11/2020.
//

import UIKit

extension CartViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType = cellsList[indexPath.row]
        
        switch cellType {
        case .address:
            let cell  = tableView.dequeueReusableCell(cellClass: AddressCell.self)
            let address = viewModel.getAllSavedAddress()[viewModel.selectedAddressIndex]
            cell.addressLabel.text = address.desc
            cell.didPressChangeAddress = { [unowned self] in
                self.showChangeAddressView()
            }
            return cell
        case .balance:
            let cell  = tableView.dequeueReusableCell(cellClass: WalletCell.self)
            return cell
        case .coupon:
            let cell  = tableView.dequeueReusableCell(cellClass: CouponCell.self)
            cell.couponValue = viewModel.promoValue
            cell.didPressChangeCoupon = { [unowned self] in
                self.showChangeCouponView()
            }
            return cell
        case .payment:
            let cell  = tableView.dequeueReusableCell(cellClass: PaymentTableCollectionCell.self)
            cell.collectionView.registerNibFor(cellClass: PaymentMethodCell.self)
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.tag = CellType.payment.rawValue
            cell.didUpdateContent = { [unowned self] in
                let index = self.cellsList.firstIndex(of: .payment) ?? 0
                self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
            }
            cell.showExtraInfo = (viewModel.selectedPaymentMethod == .visa)
            cell.collectionView.reloadData { [unowned self] in
                if self.viewModel.selectedPaymentMethod != nil {
                    let index = PaymentMethodType.allItems().firstIndex(of: self.viewModel.selectedPaymentMethod!) ?? 0
                    cell.collectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: .centeredHorizontally, animated: true)
                }

            }
            
            return cell
        case .deliveryTime:
            let cell  = tableView.dequeueReusableCell(cellClass: DeliveryTimeCell.self)
            
            cell.slotsCollectionView.registerNibFor(cellClass: DeliveryTimeSlotsCell.self)
            cell.slotsCollectionView.delegate = self
            cell.slotsCollectionView.dataSource = self
            cell.slotsCollectionView.tag = -10
            cell.slotsCollectionView.reloadData()
            
            cell.DaysCollectionView.registerNibFor(cellClass: DeliveryTimeDaysCell.self)
            cell.DaysCollectionView.delegate = self
            cell.DaysCollectionView.dataSource = self
            cell.DaysCollectionView.tag = -5
            cell.DaysCollectionView.reloadData()

            return cell
        case .item:
            let cell  = tableView.dequeueReusableCell(cellClass: CartItemCell.self)
            let firstIndex = cellsList.firstIndex(of: .item) ?? 0
            let item = cartItems[indexPath.row - firstIndex]
            
            cell.itemImage.image = UIImage(named: item.imageName ?? "")
            cell.currencyLabel.text = item.currency
            cell.priceLabel.text = item.price
            cell.itemNameLabel.text = item.title
            cell.itemDescLabel.text = item.desc
            return cell
        case .ItemHeader:
            let cell  = tableView.dequeueReusableCell(cellClass: CartItemHeaderCell.self)
            return cell
        case .showHide:
            let cell  = tableView.dequeueReusableCell(cellClass: ShowHideCell.self)
            if isCollappsed {
                cell.detailsLabel?.text = "Show".l10n()
            } else {
                cell.detailsLabel?.text = "Hide".l10n()
            }
            return cell
        default:
            let cell  = tableView.dequeueReusableCell(cellClass: AddressCell.self)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

extension CartViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == cellsList.count - 1 {
            isCollappsed = !isCollappsed
        }
    }
}
