//
//  ViewController.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 28/11/2020.
//

import UIKit
import L10n_swift
import Stellar
class SplashViewController: UIViewController {

    @IBOutlet weak var haseelLabel: UILabel!
    @IBOutlet weak var logotrainlingConstraint: NSLayoutConstraint!
    @IBOutlet var logoCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var upperDash: UIView!
    @IBOutlet weak var centeredDash: UIView!
    @IBOutlet weak var lowerDash: UIView!
    
    let duration = 2.0
    let delay = 1.5
    var window: UIWindow!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logoImage.isFlippable = false
        upperDash.isFlippable = false
        centeredDash.isFlippable = false
        lowerDash.isFlippable = false
        logoImage.startAnimating()
        haseelLabel.alpha = 0.0
        upperDash.alpha = 0.0
        centeredDash.alpha = 0.0
        lowerDash.alpha = 0.0
        Bundle.setLanguage(L10n.shared.language)
        
        self.navigationController?.navigationBar.isHidden = true
        window = UIApplication.shared.window
        
        let xPoint = (haseelLabel.frame.origin.x + haseelLabel.frame.size.width - logoImage.frame.size.width) / 2
        var dashXPoint = xPoint + 15
//        dashXPoint = (L10n.shared.language == "ar") ? (dashXPoint * -1) : dashXPoint
        logoImage.moveX(xPoint).duration(duration).easing(.easeInEaseOut).animate()
        haseelLabel.makeAlpha(1.0).duration(duration).easing(.backEaseInOut).animate()
        
        var upperDashFrame = upperDash.frame
        if (L10n.shared.language == "ar") {
            upperDashFrame.origin.x *= -1
        }
        
        upperDash.makeFrame(upperDashFrame).animate()
        
        var lowerDashFrame = lowerDash.frame
        if (L10n.shared.language == "ar") {
            lowerDashFrame.origin.x *= -1
        }
        lowerDash.makeFrame(lowerDashFrame).animate()
        
        var centeredDashFrame = centeredDash.frame
        if (L10n.shared.language == "ar") {
            centeredDashFrame.origin.x *= -1
        }
        centeredDash.makeFrame(centeredDashFrame).animate()
        
        upperDash.delay(delay).makeAlpha(1).duration(0.1).easing(.backEaseInOut)
            .then().moveX(dashXPoint).duration(1).easing(.bounceOut).animate()
        centeredDash.delay(delay).makeAlpha(1).duration(0.1).easing(.backEaseInOut)
            .then().moveX(dashXPoint - 5).duration(1).easing(.bounceOut).animate()
        lowerDash.delay(delay).makeAlpha(1).duration(0.1).easing(.backEaseInOut)
            .then().moveX(dashXPoint).duration(1).easing(.bounceOut).completion { [weak self] in
                let cardView = self?.storyboard?.instantiateViewController(withIdentifier: "mainNav") as? UINavigationController
                cardView?.heroNavigationAnimationType = .zoomSlide(direction: .right)
                cardView?.hero.isEnabled = true
                
                UIView.transition(with: (self?.window)!,
                                  duration: 0.5,
                                  options: .transitionCurlUp,
                                  animations: {
                                    self?.window.rootViewController = cardView
                                    
                },
                                  completion: nil)
            }.animate()
        
    }


}
