//
//  DeliveryTimeDataSource.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 03/12/2020.
//

import UIKit

protocol DeliveryTimeDataSource{
    func fetchAllDeliveryTime () -> DeliveryTime?
}

class LocalDeliveryTimeDataSource: DeliveryTimeDataSource {
    
    let deliveryTimeFileName = "deliveryTime"
    
    func fetchAllDeliveryTime() -> DeliveryTime? {
        let decoder = JSONDecoder()
           guard
                let url = Bundle.main.url(forResource: deliveryTimeFileName, withExtension: "json"),
                let data = try? Data(contentsOf: url),
                let time = try? decoder.decode(DeliveryTime.self, from: data)
           else {
                return nil
           }
           return time
    }
}
