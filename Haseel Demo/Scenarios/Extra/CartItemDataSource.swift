//
//  CartItemDataSource.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 03/12/2020.
//

import UIKit

protocol CartItemDataSource {
    func fetchAllItems() -> [CartItem]?
}

class LocalCartItemDataSource: CartItemDataSource {

    func fetchAllItems() -> [CartItem]? {
        let cartItemFileName = "items"
        let decoder = JSONDecoder()
           guard
                let url = Bundle.main.url(forResource: cartItemFileName, withExtension: "json"),
                let data = try? Data(contentsOf: url),
                let time = try? decoder.decode([CartItem].self, from: data)
           else {
                return nil
           }
           return time

    }
}
