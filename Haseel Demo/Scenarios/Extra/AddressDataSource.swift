//
//  CartDataSource.swift
//  Haseel Demo
//
//  Created by Mohammed Salah on 02/12/2020.
//

import UIKit

protocol AddressDataSource{
    func fetchAllAddress () -> [Address]?
}


class LocalSourceCartDataSource: AddressDataSource {
    let addressFileName = "address"
    
    func fetchAllAddress () -> [Address]? {
        let decoder = JSONDecoder()
           guard
                let url = Bundle.main.url(forResource: addressFileName, withExtension: "json"),
                let data = try? Data(contentsOf: url),
                let address = try? decoder.decode([Address].self, from: data)
           else {
                return nil
           }

           return address
    }
}
